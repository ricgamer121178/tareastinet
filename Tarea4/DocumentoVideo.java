package Archivos;

import java.util.Scanner;

public class DocumentoVideo extends Documento{
	int duracion;
	int tamanio;
	String formato;
	
	public DocumentoVideo(){
		duracion = 0;
		tamanio = 0;
		formato = null;
	}
	
	public int getDuracion(){
		return duracion;
	}
	
	public int getTamanio(){
		return tamanio;
	}
	
	public String getFormato(){
		return formato;
	}
	
	public void setDuracion(int duracion){
		this.duracion = duracion;
	}
	
	public void setTamanio(int tamanio){
		this.tamanio = tamanio;
	}
	
	public void setFormato(String formato){
		this.formato = formato;
	}
	
	public void ingresarDocumento(String nombreAutor, String tipo){
		this.nombreAutor = nombreAutor;
		this.tipo = tipo;
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese la Duracion del Video");
		int duraVideo = sc.nextInt();
		setDuracion(duraVideo);
		System.out.println("Ingrese el Tama�o del Video");
		int tamVideo = sc.nextInt();
		setTamanio(tamVideo);
		System.out.println("Ingrese el Formato del Video");
		String forVideo = sc.next();
		setFormato(forVideo);
		System.out.println("Datos Almacenados\n");
	}
	
	public void reproducir(){
		System.out.println("Imposible Reproducir, solo texto soportado");
	}
}
