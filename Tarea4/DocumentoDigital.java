package Archivos;

import java.util.Scanner;

public class DocumentoDigital extends Documento{
	int cantidadPaginas;
	int tamanio;
	String formato;
	
	public DocumentoDigital(){
		cantidadPaginas = 0;
		tamanio = 0;
		formato = null;
	}
	
	public int getCantidadPaginas(){
		return cantidadPaginas;
	}
	
	public int getTamanio(){
		return tamanio;
	}
	
	public String getFormato(){
		return formato;
	}
	
	public void setCantidadPaginas(int cantidadPaginas){
		this.cantidadPaginas = cantidadPaginas;
	}
	
	public void setTamanio(int tamanio){
		this.tamanio = tamanio;
	}
	
	public void setFormato(String formato){
		this.formato = formato;
	}
	
	public void ingresarDocumento(String nombreAutor, String tipo){
		this.nombreAutor = nombreAutor;
		this.tipo = tipo;
		Scanner sc2 = new Scanner(System.in);
		System.out.println("Ingrese Cantidad de Paginas");
		int cantPag = sc2.nextInt();
		setCantidadPaginas(cantPag);
		System.out.println("Ingrese Tama�o en Bytes");
		int  tamBytes = sc2.nextInt();
		setTamanio(tamBytes);
		System.out.println("Ingrese Formato");
		String forma = sc2.next();
		setFormato(forma);
		System.out.println("Datos Almacenados\n");
	}
	
	public void reproducir(){
		System.out.println("\nCantidad de Paginas : "+getCantidadPaginas());
		System.out.println("Tama�o : "+ getTamanio());
		System.out.println("Formato : "+ getFormato());
	}
}
