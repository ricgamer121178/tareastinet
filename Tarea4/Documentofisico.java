package Archivos;

import java.util.Scanner;

public class Documentofisico extends Documento {
	int cantidadPaginas;
	int anioImpresion;
	String ubicacionGeo;
	
	public Documentofisico(){
		cantidadPaginas = 0;
		anioImpresion = 0;
		ubicacionGeo = null;
	}
	
	public int getCantidadPaginas(){
		return cantidadPaginas;
	}
	
	public int getAnioImpresion(){
		return anioImpresion;
	}
	
	public String getUbicacionGeo(){
		return ubicacionGeo;
	}
	
	public void setCantidadPaginas(int cantidadPaginas){
		this.cantidadPaginas = cantidadPaginas;
	}
	
	public void setAnioImpresion(int anioImpresion){
		this.anioImpresion = anioImpresion;
	}
	
	public void setUbicacionGeo(String ubicacionGeo){
		this.ubicacionGeo = ubicacionGeo;
	}
	
	
	
	public void ingresarDocumento(String nombreAutor, String tipo){
		this.nombreAutor = nombreAutor;
		this.tipo = tipo;
		System.out.println("Ingrese Cantidad de Paginas");
		Scanner sc1 = new Scanner(System.in);
		int cantPag = sc1.nextInt();
		setCantidadPaginas(cantPag);  
		System.out.println("Ingrese A�o de Impresion");
		int anImpres = sc1.nextInt();
		setAnioImpresion(anImpres);
		System.out.println("Ingrese Donde se Encuentra");
		String ubiGeo = sc1.next().toString();
		setUbicacionGeo(ubiGeo);
		System.out.println("Documento Almacenado\n");
	}
	
	public void reproducir(){
		//System.out.println("\nCantidad de Paginas : "+getCantidadPaginas());
		//System.out.println("A�o de Impresion : "+getAnioImpresion());
		System.out.println("Imposible Reproducir, por favor dirijase a la ubicacion  "+ getUbicacionGeo() +" para recuperar la copia y leerla");
	}
	
	public void buscarArchivo(){
		
	}
	
}
