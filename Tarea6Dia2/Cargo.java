package organigrama.Tarea6;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Cargo {
	String cargoPadre;
	String nuevoCargo;
	List<Empleado> empleados;
	Map<String, String> mapaAuxiliar;
	List<Cargo> cargosHijos = new ArrayList<Cargo>();
	Cargo mismoCargo;
	
	
	
	public Cargo(){
		this.cargoPadre = null;
		this.nuevoCargo = null;
	}
	
	public List<Cargo> getCargosHijos() {
		return cargosHijos;
	}

	public void setCargosHijos(List<Cargo> cargosHijos) {
		this.cargosHijos = cargosHijos;
	}

	
	public Cargo getMismoCargo() {
		return mismoCargo;
	}

	public void setMismoCargo(Cargo mismoCargo) {
		this.mismoCargo = mismoCargo;
	}
	
	public String getCargoPadre(){
		return this.cargoPadre;
	}
	
	public String getNuevoCargo(){
		return this.nuevoCargo;
	}
	
	public void setCargoPadre(String cargoPadre){
		this.cargoPadre = cargoPadre;
	}
	
	public void setNuevoCargo(String nuevoCargo){
		this.nuevoCargo = nuevoCargo;
	}
	
	
	
	public void mostrarListaHijos(){
		for(int i = 0;i < cargosHijos.size();i++ ){
			System.out.println(cargosHijos.get(i));
		}
	}
	
	public void crearEmpleado(String cargo, Empleado empleado){
		if(empleados == null){
			empleados = new ArrayList<Empleado>();
		}
		if(mapaAuxiliar == null){
			mapaAuxiliar = new HashMap<String, String>();
		}
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese ID del empleado : ");
		String idEmpleado = sc.next();
		empleado.setId(idEmpleado);
		System.out.println("Ingrese el nombre del empleado : ");
		String nombreEmpleado = sc.next();
		empleado.setNombre(nombreEmpleado);
		System.out.println("Ingrese la edad : ");
		String edad = sc.next();
		//empleado.setEdad(edad);
		mapaAuxiliar.put("edad", edad);
		System.out.println("Ingrese el sexo : ");
		String sexo = sc.next();
		//empleado.setSexo(sexo);
		mapaAuxiliar.put("sexo", sexo);
		empleado.setCaracteristicas(mapaAuxiliar);
		empleados.add(empleado);
	}
	
	public void mostrarEmpleados(){
		for(int i = 0; i < empleados.size();i++){
			System.out.println(empleados.get(i));
		}
	}

	@Override
	public String toString() {
		/*return "Cargo [ cargoPadre=" + cargoPadre
				+ ", nuevoCargo=" + nuevoCargo + "]";*/
		return nuevoCargo;
	}

	
	
}
