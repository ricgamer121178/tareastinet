package organigrama.Tarea6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Organigrama {
	private Map<String, Cargo> cargos;
	private List<Cargo> listaCargos;
	private Cargo nuevoCargo;
	Cargo cargo = new Cargo();
	
	
	public List<Cargo> getListaCargos() {
		return listaCargos;
	}


	public void setListaCargos(List<Cargo> listaCargos) {
		this.listaCargos = listaCargos;
	}
	
	public Map<String, Cargo> getCargos() {
		return cargos;
	}


	public void setCargos(Map<String, Cargo> cargos) {
		this.cargos = cargos;
	}
	
	public void generarPadreOrganigrama(String cargoPadre, String cargoHijo){
		if(cargos == null){
			cargos = new HashMap<String, Cargo>();
		}
		
		cargo.setCargoPadre(cargoPadre);
		cargo.setNuevoCargo(cargoHijo);
		cargos.put(cargoPadre, cargo);
	}


	public void insertarCargo(String cargoPadre, String nuevoCargo){
		if(cargos == null){
			cargos = new HashMap<String, Cargo>();
			System.out.println("entra aca");
		}
		
		
		if(cargos.containsKey(nuevoCargo)){
			System.out.println("Este cargo ya existe");
		}else{
			cargo.setCargoPadre(cargoPadre);
			cargo.setNuevoCargo(nuevoCargo);
			cargos.put(nuevoCargo, cargo);
		}
		
		if(recuperarCargo(cargoPadre) == null){
			System.out.println("Cargo padre no existe");
		}else{
			Cargo cargoPapa = recuperarCargo(cargoPadre);
			//cargoPapa.inicializarListaHijos();
			List<Cargo> hijosCargo = new ArrayList<Cargo>();
			hijosCargo = cargoPapa.getCargosHijos();
			System.out.println(cargoPapa.getCargosHijos());
			if(hijosCargo == null){
				System.out.println("Esta vacia");
				
			}
			List<Cargo> listaHijos = cargoPapa.getCargosHijos();
			listaHijos.add(cargoPapa);
			cargoPapa.setCargosHijos(listaHijos);
		}
		
	}
	
	public Cargo recuperarCargo(String cargoBuscado){
		return cargos.get(cargoBuscado);
	}

	
	public void mostrarTodo(){
		for(Map.Entry me : cargos.entrySet()){
			//System.out.println(me.getKey()+" "+me.getValue());
			System.out.println(me.getValue());
		}
	}
	
	public void mostrarElemento(String elemento){
		Cargo otrocargo = cargos.get(elemento);
		//nuevoCargo = cargos.get(elemento);
		System.out.println(otrocargo);
	}
	
}
