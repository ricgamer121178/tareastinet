package DB;

public class Nodo {
	private String valor;
	private Nodo sig;
	
	public Nodo(){
		this.valor = null;
		this.sig = null;
	}
	
	public void setValor(String valor){
		this.valor = valor;
	}
}
