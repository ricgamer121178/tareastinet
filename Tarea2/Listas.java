package ProyectoTarea2;

import java.util.Scanner;
import java.sql.SQLException;

public class Listas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		
		Funciones fun = new Funciones();
		int leer = 0;
		try{
			do{
				System.out.println("Sistema de Listas\n");
				System.out.println("1.- Agregar Elemento a la Lista");
				System.out.println("2.- Obtener la posicion de un Elemento en la Lista");
				System.out.println("3.- Eliminar a un Elemento de la Lista");
				System.out.println("4.- Insertar un Elemento en una posicion especifica en la Lista");
				System.out.println("5.- Cantidad de Elementos en la Lista");
				System.out.println("6.- Listado de Elementos");
				System.out.println("7.- Salir");
				leer = sc.nextInt();
				boolean bandError = true;
				switch(leer){
				case 1:
					do{
						try{
							System.out.println("Ingrese un Elemento a la Lista : ");
							Scanner sc1 = new Scanner(System.in);
							int agregar = sc1.nextInt();
							fun.agregarAlFinal(agregar);
							System.out.println("\n");
							bandError = false;
							//fun.listar();
						}catch(Exception nfe){
							System.err.println("Debe Ingresar solo numeros");
						}
					}while(bandError);
					break;
				case 2:
					do{
						try{
							System.out.println("Ingrese la Posicion del Elemento que Busca : ");
							Scanner sc2 = new Scanner(System.in);
							int buscar = sc2.nextInt();
							//fun.getValor(buscar);
							int valorEncontrado = fun.getValor(buscar);
							System.out.println("El Valor del Elemento en la Posicion "+ buscar +" es : " + valorEncontrado);
							System.out.println("\n");
							bandError = false;
						}catch(Exception ee){
							System.err.println("Debe ingresar solo numeros");
						}
					}while(bandError);
					break;
				case 3:
					do{
						try{
							System.out.println("Ingrese la Posicion del Elemento a Eliminar : ");
							Scanner sc3 = new Scanner(System.in);
							int eliminar = sc3.nextInt();
							fun.eliminarElemento(eliminar);
							System.out.println("\n");
							//fun.listar();
							bandError = false;
						}catch(Exception ee){
							System.err.println("Debe ingresar solo numeros");
						}
					}while(bandError);
					break;
				case 4:
					do{
						try{
							System.out.println("Ingrese la Posicion en cual Quiere Ingresar al Elemento :");
							Scanner sc4 = new Scanner(System.in);
							int posi = sc4.nextInt();
							System.out.println("Ahora Ingrese el Elemento : ");
							Scanner sc5 = new Scanner(System.in);
							int ele = sc5.nextInt();
							fun.ingresarEnPosInd(posi, ele);
							System.out.println("\n");
							//fun.listar();
							bandError = false;
						}catch(Exception ee){
							System.err.println("Debe ingresar solo numeros");
						}
					}while(bandError);
					break;
				case 5:
					int cantEle = fun.cantElementos();
					System.out.println("Cantidad de Elementos que Contiene la Lista : " + cantEle);
					System.out.println("\n");
					break;
				case 6:
					System.out.println("Listado de Elementos que contiene la lista : ");
					fun.listar();
					System.out.println("\n");
					break;
				case 7:
					System.out.println("Gracias por Utilizar este Programa, adios");
					System.exit(0);
					break;
				}
			}while(leer > 0 && leer < 8);
		}catch(Exception ee){
			System.err.println("Debe ingresar solo numeros");
		}
		//fun.agregarAlFinal(4);
		
		//fun.listar();
	}

}
