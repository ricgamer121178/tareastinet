package ProyectoTarea2;

public class Nodos {
	private int valor;
	private Nodos Sig;
	
	public Nodos(){
		this.valor = 0;
		this.Sig = null;
	}
	
	public int getValor(){
		return valor;
	}
	
	public void setValor(int valor){
		this.valor = valor;
	}
	
	public Nodos getSig(){
		return Sig;
	}
	
	public void setSig(Nodos Sig){
		this.Sig = Sig;
	}
}
