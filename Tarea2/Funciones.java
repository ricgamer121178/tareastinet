package ProyectoTarea2;

public class Funciones {
	private Nodos inicio;
	private int tam;
	
	public Funciones(){
		this.inicio = null;
		this.tam = 0;
	}
	
	public int getTam(){
		return tam;
	}
	
	public boolean esVacia(){
		return inicio == null;
	}
	
	public void agregarAlFinal(int valor){
		Nodos nuevo = new Nodos();
		nuevo.setValor(valor);
		if(esVacia()){
			inicio = nuevo;
		}else{
			Nodos listAux = inicio;
			while(listAux.getSig() != null){
				listAux = listAux.getSig();
			}
			listAux.setSig(nuevo);
		}
		tam++;
	}
	
	public void eliminarElemento(int pos){
		if(pos >=0 && pos < tam){
			if(pos == 0){
				inicio = inicio.getSig();
			}else{
				Nodos listAux = inicio;
				for(int i = 0; i < pos -1; i++){
					listAux = listAux.getSig();
				}
				
				Nodos Sig = listAux.getSig();
				listAux.setSig(Sig.getSig());
			}
			tam--;
		}
	}
	
	public int getValor(int pos){
		if(pos == 0){
			return inicio.getValor();
		}else{
			Nodos listAux = inicio;
			for(int i = 0;i < pos; i++){
				listAux = listAux.getSig();
			}
			
			return listAux.getValor();
		}
	}
	
	public void ingresarEnPosInd(int pos, int valor){
		if(pos >= 0 && pos <= tam){
			Nodos nodo = new Nodos();
			nodo.setValor(valor);
			if(pos == 0){
				nodo.setSig(inicio);
				inicio = nodo;
			}else{
				if(pos == tam){
					Nodos listAux = inicio;
					while(listAux.getSig() != null){
						listAux = listAux.getSig();
					}
					listAux.setSig(nodo);
				}else{
					Nodos listAux = inicio;
					for(int i = 0;i < (pos -1);i++){
						listAux = listAux.getSig();
					}
					Nodos Sig = listAux.getSig();
					listAux.setSig(nodo);
					nodo.setSig(Sig);
				}
			}
		}else{
			System.out.println("No entra aca");
		}
		tam++;
	}
	
	public int cantElementos(){
		return tam;
	}
	
	public void listar(){
		if(!esVacia()){
			Nodos listAux = inicio;
			int i = 0;
			while(listAux != null){
				System.out.println(i + " .[ " + listAux.getValor() + " ] " );
				listAux = listAux.getSig();
				i++;
			}
		}else{
			System.out.println("La lista est� vac�a");
		}
	}

}
