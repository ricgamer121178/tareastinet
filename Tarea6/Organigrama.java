package organigrama.Tarea6;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Organigrama {
	private Map<String, Cargo> cargos;
	
	
	public Map<String, Cargo> getCargos() {
		return cargos;
	}


	public void setCargos(Map<String, Cargo> cargos) {
		this.cargos = cargos;
	}


	public void insertarCargo(String cargoPadre, String nuevoCargo){
		if(cargos == null){
			cargos = new HashMap<String, Cargo>();
		}
		Cargo cargo = new Cargo();
		cargo.setCargoPadre(cargoPadre);
		cargo.setNuevoCargo(nuevoCargo);
		cargos.put(nuevoCargo, cargo);
		
		
	}
	
	public String recuperarCargo(String cargoBuscado){
		return cargos.get(cargoBuscado).toString();
	}

	
	public void mostrarTodo(){
		for(Map.Entry me : cargos.entrySet()){
			//System.out.println(me.getKey()+" "+me.getValue());
			System.out.println(me.getValue());
		}
	}
	
}
