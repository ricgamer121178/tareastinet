package organigrama.Tarea6;

import java.util.Scanner;

public class App 
{
    public static void main( String[] args )
    {
        Scanner sc = new Scanner(System.in);
        Organigrama organigrama = new Organigrama();
        Cargo cargo = new Cargo();
        Empleado empleado = new Empleado();
        int opcion;
        do{
        	System.out.println("Ingreso de Organigrama\n");
        	System.out.println("1.- Ingreso de Cargo");
        	System.out.println("2.- Ingreso de Empleado");
        	System.out.println("3.- Listar");
        	System.out.println("4.- Salir");
        	opcion = sc.nextInt();
        	switch(opcion){
        	case 1:
        		
        		Scanner sc1 = new Scanner(System.in);
        		System.out.println("Ingrese el Cargo del Padre");
        		String cargoPadre = sc1.next();
        		System.out.println("Ingrese el cargo del Hijo");
        		String nuevoCargo = sc1.next();
        		organigrama.insertarCargo(cargoPadre, nuevoCargo);
        		//organigrama.recuperarCampo();
        		break;
        	case 2:
        		Scanner sc2 = new Scanner(System.in);
        		System.out.println("Ingrese el cargo del empleado");
        		String cargoEmpleado = sc2.next();
        		cargo.crearEmpleado(cargoEmpleado, empleado);
        		break;
        	case 3:
        		System.out.println("Todos los cargos");
        		organigrama.mostrarTodo();
        		cargo.mostrarEmpleados();
        		break;
        	case 4:
        		System.out.println("Gracias por utilizar este programa");
        		System.exit(0);
        		break;
        	}
       }while(opcion != 4);
    }
}
