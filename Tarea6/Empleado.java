package organigrama.Tarea6;

import java.util.Map;
import java.util.HashMap;

public class Empleado{
	Map<String, String> caracteristicas;
	String id;
	String nombre;
	String edad;
	String sexo;
	
	public Empleado(){
		this.id = null;
		this.nombre = null;
		this.edad = null;
		this.sexo = null;
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getNombre(){
		return this.nombre;
	}
	
	public String getEdad(){
		return this.edad;
	}
	
	public String getSexo(){
		return this.sexo;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public void setEdad(String edad){
		this.edad = edad;
	}
	
	public void setSexo(String sexo){
		this.sexo = sexo;
	}
	
	public Map<String, String> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(Map<String, String> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}
	
	@Override
	public String toString() {
		return "Empleado [  id=" + id
				+ ", nombre=" + nombre	+ ", caracteristicas=" + caracteristicas + "]";
	}

	
}
