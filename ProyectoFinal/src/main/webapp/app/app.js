// Creación del módulo Principal
//se importa el modulo ui.router
angular.module("app", ['ui.router','ngResource'])
.config(function($stateProvider, $urlRouterProvider) {
       //Por defecto se mostrara el estado app, que contiene el listado de productos
        $urlRouterProvider.otherwise("/app");
         //definición de estados
         $stateProvider
         //estado de la página principal
           .state('app', {
                   url: "/app" ,
                   templateUrl: "/app/productos/principal.tpl.html",
                   controller : 'PrincipalController'
           })
           //estado para la creación de un nuevo usuario
          .state('usuarionuevo', {
                   url: "/nuevo" ,
                   templateUrl: "/app/usuario/agregarUsuario.tpl.html",
                   controller : 'AgregarUsuarioController'
           })
           //estado para el login(no programado aún)
           .state('login', {
                   url: "/login" ,
                   templateUrl: "/app/usuario/loginUsuario.tpl.html",
                   controller : 'AgregarUsuarioController'
           })
           //estado para el carrito de compras
           .state('carrito', {
                   url: "/carrito" ,
                   templateUrl: "/app/compras/carrito.tpl.html",
                   controller : 'CarritoController'
           })
    }
);