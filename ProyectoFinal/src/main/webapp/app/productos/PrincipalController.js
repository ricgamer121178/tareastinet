angular.module("app")
.controller('PrincipalController',PrincipalController);
//Esta línea sirve para declarar los objetos que se inyectarán al controller
PrincipalController.$inject = ['$scope', 'ProductosFactory','$stateParams'];
//Definicion del controller
function PrincipalController($scope, Productos, $stateParams){
       //arreglo para contener datos del formulario de creacion
       $scope.listado = [];
       
       //se vacía el carrito una vez que ya se realizó la compra
       $scope.myCart.length = 0;
       
       //se muestran los productos al inicio
       refrescarlistado();
       
       //función para obtener los productos
       function refrescarlistado(){
         $scope.listado = Productos.query();
         
       }
       
       //Se llena el carrito myCart en la pantalla donde están todos los productos
       $scope.addToCart = function(item){
    	   if((objectFindByKey($scope.myCart, 'idProducto', item.id)) == null){
    		   $scope.valorTotal = $scope.valorTotal + item.precio;
        	   $scope.myCart.push(
        		{
        			"idProducto" : item.id,
        			"nombreProducto" : item.nombre,
        			"cantidad" : 1,
        			"stock" : item.cantidad,
        			"precioProducto" : item.precio,
        			"precioTotal" : item.precio,
        		}	   
        	   );
        	   $scope.contador += 1;
    	   }else{
    		   var indice = objectFindByKey($scope.myCart, 'idProducto', item.id);
    		   if($scope.myCart[indice].cantidad < item.cantidad){
	    		   $scope.myCart[indice].cantidad += 1;
	    		   $scope.myCart[indice].precioTotal += item.precio;
	    		   $scope.valorTotal = $scope.valorTotal + item.precio;
	    		   $scope.contador += 1;
    		   }else{
    			   alert("No hay mas en stock");
    		   }
    	   }
    	     
    			   
       };
       
       //función para obtener el índice de un producto ya guardado en el carrito
       function objectFindByKey(array, key, value) {
    	    for (var i = 0; i < array.length; i++) {
    	        if (array[i][key] === value) {
    	        	return i;
    	        }
    	    }
    	    return null;
    	}
       
}