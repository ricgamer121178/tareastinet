angular.module("app")
.controller('IndexController',IndexController);
//Esta línea sirve para declarar los objetos que se inyectarán al controller
IndexController.$inject = ['$scope', '$stateParams'];
//Definicion del controller
function IndexController($scope,  $stateParams){
	
	//se inician el contador mas el valor total para que se puedan ver desde los demás controladores
	$scope.contador = 0;
    $scope.valorTotal = 0;
    
    //se inicia el carrito myCart acá para que se pueda ver desde los demás controladores
    $scope.myCart=[];
    
}