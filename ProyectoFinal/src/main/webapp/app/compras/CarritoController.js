angular.module("app")
.controller('CarritoController',CarritoController);
//Esta línea sirve para declarar los objetos que se inyectarán al controller
CarritoController.$inject = ['$scope', 'CarritoFactory','$stateParams', '$state'];
//Definicion del controller
function CarritoController($scope, Carrito, $stateParams, $state){
	
		//objeto para contener el carrito lleno 
		//se opta por traspasar la lista a un objeto ya que eso es lo que se recibe en la parte Java
		$scope.carrito = {};
       
		//se llama al inicio para que el total esté disponible apenas se ingrese acá
		valorDelCarro();
		
		//función para obtener el total de todo el carrito
		function valorDelCarro(){
			$scope.totalPagar = 0;
			for(var i = 0; i < $scope.myCart.length; i++){
				$scope.totalPagar += $scope.myCart[i].precioTotal;
			}
		}
		
		//se va agregando de a un producto al carrito
		//se valida que no se pueda agregar más de lo que hay en el stock
		$scope.sumarProducto = function(item){
			var indice = objectFindByKey($scope.myCart, 'idProducto', item.idProducto);
			if($scope.myCart[indice].stock > item.cantidad){
	 		    $scope.myCart[indice].cantidad += 1;
	 		    $scope.myCart[indice].precioTotal += item.precioProducto;
	 		    valorDelCarro();
			}else{
				alert("No hay mas en stock");
			}
		};
		
		//se va quitando de a un producto al carrito
		//se valida que si la cantidad llega a cero, se elimine el producto de el carrito
		$scope.quitarProducto = function(item){
			var indice = objectFindByKey($scope.myCart, 'idProducto', item.idProducto);
			if($scope.myCart[indice].cantidad > 0){
	 		    $scope.myCart[indice].cantidad -= 1;
	 		    $scope.myCart[indice].precioTotal -= item.precioProducto;
	 		    valorDelCarro();
			}
			if($scope.myCart[indice].cantidad == 0){
				$scope.eliminarProducto($scope.myCart[indice]);
			}
		};
		
		//se elimina el producto de el carrito
		$scope.eliminarProducto = function(item){
			var indice = objectFindByKey($scope.myCart, 'idProducto', item.idProducto);
			$scope.myCart.splice(indice, 1);
			valorDelCarro();
		}
		
		//función para obtener el índice de un producto ya guardado en el carrito
		function objectFindByKey(array, key, value) {
    	    for (var i = 0; i < array.length; i++) {
    	        if (array[i][key] === value) {
    	        	return i;
    	        }
    	    }
    	    return null;
    	}
		
		//se guarda el carrito previamente habiendolo pasado a un objeto
		//ya que eso es lo que espera la parte Java
		$scope.save = function(){
			$scope.carrito.listaCompras = $scope.myCart;
			user = new Carrito($scope.carrito);
			user.$save();
			$state.go("app");
		}
		
}