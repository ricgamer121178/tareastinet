angular.module("app")
.controller('AgregarUsuarioController',AgregarUsuarioController);
//Esta línea sirve para declarar los objetos que se inyectarán al controller
AgregarUsuarioController.$inject = ['$scope', 'UsuarioFactory','$stateParams','$state'];
//Definicion del controller
function AgregarUsuarioController($scope, Usuario, $stateParams, $state){
       
		//arreglo para contener los usuarios
		$scope.listadoUsuarios = [];
		
		//se llama para que estén disponibles al inicio
		refrescarlistado();
		
		//función que obtiene todos los usuarios
		//la idea era validar que no se ingresara el mismo usuario
		function refrescarlistado(){
			$scope.listadoUsuarios = Usuario.query();
		}
	
		//se almacena el usuario
		$scope.save = function(){
             user = new Usuario($scope.formAgregar);
             user.$save();
             $state.go('app');
       }
       
                
}