angular.module("app").factory('UsuarioFactory', UsuarioFactory);
//Declaracion de recursos que se deben inyectar.
UsuarioFactory.$inject = ['$resource'];
function UsuarioFactory($resource) {
//Creamos un objeto del tipo $resource, con la URI de nuestra api REST,
       //el objeto retornado nos permite llamar a los métodos
       //query() : Realiza una solicitud a la URI utilizando el método GET
      //(sin enviar parámetros)
       //get({id: 4}) : Realiza una solicitud GET enviando el id como parámetro.
       //
       //Además los objetos retornados vienen con los métodos $save() y $delete
       //que llaman a la api rest con los métodos POST o DELETE respectivamente.
       //
       //Se pueden definir métodos propios para otras operaciones, por ejemplo acá se
       //define que los objetos retornados tendrán el método edit(), que al llamarse
       //realizara una solicitud a la API usando el método PUT
       return $resource('/usuarios/:id', {id:'@id'}, {'edit': {method: 'PUT'}})
}