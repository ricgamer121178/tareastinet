package tinet.tutoriales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import tinet.tutoriales.entidades.Productos;

//repositorio para la entidad de Productos
public interface ProductosRepository extends JpaRepository<Productos, Long> {

}
