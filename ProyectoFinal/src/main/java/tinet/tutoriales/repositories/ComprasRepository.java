package tinet.tutoriales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import tinet.tutoriales.entidades.Compras;

//repositorio para la entidad Compras
public interface ComprasRepository extends JpaRepository<Compras, Long> {
	
}
