package tinet.tutoriales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import tinet.tutoriales.entidades.Usuario;

//repositorio para la entidad Usuario
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	
}
