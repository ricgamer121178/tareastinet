package tinet.tutoriales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import tinet.tutoriales.entidades.ComprasProductos;

//repositorio para la entidad ComprasProductos donde se pretendía
//hacer las querys nativas para las relaciones
public interface ComprasProductosRepository extends JpaRepository<ComprasProductos, Long> {

}
