package tinet.tutoriales.to;

import java.util.List;

import tinet.tutoriales.entidades.Compras;

public class ComprasTO {
	
	private List<Compras> listaCompras;

	//se creo un TO con la idea de que se pudiera recibir un objeto
	//en lugar de una lista al momento de guardar el carrito
	public List<Compras> getListaCompras() {
		return listaCompras;
	}

	public void setListaCompras(List<Compras> listaCompras) {
		this.listaCompras = listaCompras;
	}
	

}
