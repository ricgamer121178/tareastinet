package tinet.tutoriales.entidades;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

//entidad que se utiliza para guardar las compras o carrito
//las variables comentadas iban a ser utilizadas para la relación entre tablas
@Entity
public class Compras {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
//	private String nombre;
//	private String estado;
	private long idProducto;
	private String nombreProducto;
	private int cantidad;
	private long precioProducto;
	private long precioTotal;
	

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public long getPrecioProducto() {
		return precioProducto;
	}
	public void setPrecioProducto(long precioProducto) {
		this.precioProducto = precioProducto;
	}
	public long getPrecioTotal() {
		return precioTotal;
	}
	public void setPrecioTotal(long precioTotal) {
		this.precioTotal = precioTotal;
	}
}
