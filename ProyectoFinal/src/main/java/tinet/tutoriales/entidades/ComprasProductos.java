package tinet.tutoriales.entidades;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

//entidad que estaba destinada a romper la relación many to many
@Entity
public class ComprasProductos {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "idCompras", referencedColumnName="id")
	private Compras idCompras;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "idProducto", referencedColumnName="id")
	private Productos idProductos;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Compras getIdCompras() {
		return idCompras;
	}

	public void setIdCompras(Compras idCompras) {
		this.idCompras = idCompras;
	}

	public Productos getIdProductos() {
		return idProductos;
	}

	public void setIdProductos(Productos idProductos) {
		this.idProductos = idProductos;
	}

	
	

	

}
