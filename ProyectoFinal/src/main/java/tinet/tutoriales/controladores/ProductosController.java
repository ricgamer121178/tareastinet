package tinet.tutoriales.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.entidades.Productos;
import tinet.tutoriales.repositories.ProductosRepository;

@RestController
public class ProductosController {

	@Autowired
	ProductosRepository productosRepository;
	
	//guardar producto
	@RequestMapping(value="/productos", method=RequestMethod.POST)
    public void guardarProducto(@RequestBody Productos p){
		productosRepository.save(p);
    }
	
	//obtener producto
	@RequestMapping(value="/productos/{id}", method=RequestMethod.GET)
	public Productos obtenerProducto(@PathVariable long id){
		return productosRepository.findOne(id);
	}
	
	//obtener todos los productos
	@RequestMapping(value="/productos", method=RequestMethod.GET)
	public List<Productos> obtenerProductos(){
		return productosRepository.findAll();
	}
	
	//eliminar producto
    @RequestMapping(value="/productos/{id}", method=RequestMethod.DELETE)
    public void eliminarProducto(@PathVariable long id){
    	productosRepository.delete(id);
    }     
   
    //modificar producto
    @RequestMapping(value="/productos/{id}", method=RequestMethod.PUT)
    public void modificarProducto(@RequestBody Productos p){
          Productos encontrado = productosRepository.findOne(p.getId());
          encontrado.setNombre(p.getNombre());
          encontrado.setPrecio(p.getPrecio());
          encontrado.setCantidad(p.getCantidad());
          productosRepository.save(encontrado);
    }    

}
