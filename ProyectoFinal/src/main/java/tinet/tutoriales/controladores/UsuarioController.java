package tinet.tutoriales.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.entidades.Usuario;
import tinet.tutoriales.repositories.UsuarioRepository;

@RestController
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	//guardar usuario
		@RequestMapping(value="/usuarios", method=RequestMethod.POST)
	    public void guardarUsuario(@RequestBody Usuario u){
			usuarioRepository.save(u);
	    }
		
		//obtener usuario
		@RequestMapping(value="/usuarios/{id}", method=RequestMethod.GET)
		public Usuario obtenerUsuario(@PathVariable long id){
			return usuarioRepository.findOne(id);
		}
		
		//obtener todos los usuarios
		@RequestMapping(value="/usuarios", method=RequestMethod.GET)
		public List<Usuario> obtenerUsuarios(){
			return usuarioRepository.findAll();
		}
		
		//eliminar usuario
	    @RequestMapping(value="/usuarios/{id}", method=RequestMethod.DELETE)
	    public void eliminarUsuario(@PathVariable long id){
	    	usuarioRepository.delete(id);
	    }     
	   
	    //modificar usuario
	    @RequestMapping(value="/usuarios/{id}", method=RequestMethod.PUT)
	    public void modificarUsuario(@RequestBody Usuario u){
	          Usuario encontrado = usuarioRepository.findOne(u.getId());
	          encontrado.setNombreUsuario(u.getNombreUsuario());
	          encontrado.setPassword(u.getPassword());
	          encontrado.setNombre(u.getNombre());
	          encontrado.setApellido(u.getApellido());
	          usuarioRepository.save(encontrado);
	    }   

}
