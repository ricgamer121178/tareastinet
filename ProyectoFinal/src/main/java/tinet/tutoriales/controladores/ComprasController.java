package tinet.tutoriales.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.entidades.Compras;
import tinet.tutoriales.repositories.ComprasRepository;
import tinet.tutoriales.repositories.ProductosRepository;
import tinet.tutoriales.to.ComprasTO;

@RestController
public class ComprasController {
	
	@Autowired
	ComprasRepository comprasRepository;
	ProductosRepository productosRepository;
	
	//guardar compra, se utiliza un TO ya que lo se espera es un objeto
		@RequestMapping(value="/compras", method=RequestMethod.POST)
		public void guardarCompra(@RequestBody ComprasTO compra){
			for(Compras compras : compra.getListaCompras()){
				comprasRepository.save(compras);
			}
	    }
		
		//obtener compra
		@RequestMapping(value="/compras/{id}", method=RequestMethod.GET)
		public Compras obtenerCompra(@PathVariable long id){
			return comprasRepository.findOne(id);
		}
		
		//obtener todos las compras
		@RequestMapping(value="/compras", method=RequestMethod.GET)
		public List<Compras> obtenerCompras(){
			return comprasRepository.findAll();
		}
		
		//eliminar compra
	    @RequestMapping(value="/compras/{id}", method=RequestMethod.DELETE)
	    public void eliminarCompra(@PathVariable long id){
	    	comprasRepository.delete(id);
	    }     
	   
	    //modificar compra(quedó comentada la funcionalidad mientras se trataba de
	    //darle funcionalidad a la tabla intermediaria(ComprasProductos))
	    @RequestMapping(value="/compras/{id}", method=RequestMethod.PUT)
	    public void modificarCompra(@RequestBody Compras compra){
//	          Compras encontrado = comprasRepository.findOne(compra.getId());
//	          encontrado.setIdProducto(compra.getIdProducto());
//	          encontrado.setNombreProducto(compra.getNombreProducto());
//	          encontrado.setCantidad(compra.getCantidad());
//	          encontrado.setPrecioProducto(compra.getPrecioProducto());
//	          encontrado.setPrecioTotal(compra.getPrecioTotal());
	         // comprasRepository.save(encontrado);
	    }    


}
