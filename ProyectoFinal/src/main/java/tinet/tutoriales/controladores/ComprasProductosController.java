package tinet.tutoriales.controladores;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.entidades.Compras;
import tinet.tutoriales.entidades.ComprasProductos;
import tinet.tutoriales.repositories.ComprasProductosRepository;
import tinet.tutoriales.to.ComprasTO;

@RestController
public class ComprasProductosController {
	
	//la función de este controller iba a ser la de darle funcionalidad
	//a la tabla intermedia que rompe el many to many
	ComprasProductosRepository comprasProductosRepository;
	/*
	//guardar compra
			@RequestMapping(value="/compras", method=RequestMethod.POST)
		    public void guardarCompra(@RequestBody ComprasProductos compra){
					comprasProductosRepository.save(compra);
		    }
			
			//obtener compra
			@RequestMapping(value="/compras/{id}", method=RequestMethod.GET)
			public ComprasProductos obtenerCompra(@PathVariable long id){
				return comprasProductosRepository.findOne(id);
			}
			
			//obtener todos las compras
			@RequestMapping(value="/compras", method=RequestMethod.GET)
			public List<ComprasProductos> obtenerCompras(){
				return comprasProductosRepository.findAll();
			}
			
			//eliminar compra
		    @RequestMapping(value="/compras/{id}", method=RequestMethod.DELETE)
		    public void eliminarCompra(@PathVariable long id){
		    	comprasProductosRepository.delete(id);
		    }     
		   
		    //modificar compra
		    @RequestMapping(value="/compras/{id}", method=RequestMethod.PUT)
		    public void modificarCompra(@RequestBody ComprasProductos compra){
		          /*Compras encontrado = comprasProductosRepository.findOne(compra.getId());
		          encontrado.setIdProducto(compra.getIdProducto());
		          encontrado.setNombreProducto(compra.getNombreProducto());
		          encontrado.setCantidad(compra.getCantidad());
		          encontrado.setPrecioProducto(compra.getPrecioProducto());
		          encontrado.setPrecioTotal(compra.getPrecioTotal());
		          comprasRepository.save(encontrado);
		    }    

*/
}
