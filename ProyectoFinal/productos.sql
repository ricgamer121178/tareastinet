-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2016 at 03:58 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carrito`
--

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id` bigint(20) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`id`, `cantidad`, `nombre`, `precio`) VALUES
(1, 10, 'lcd AOC', 200000),
(2, 10, 'lcd Samsung', 250000),
(3, 10, 'PlayStation 4', 220000),
(4, 10, 'macbook pro', 700000),
(5, 10, 'notebook dell', 500000),
(6, 10, 'xbox one', 250000),
(7, 10, 'dd western', 50000),
(8, 10, 'wiiU', 190000),
(9, 10, 'audifonos apple', 20000),
(10, 10, 'impresora hp', 40000),
(11, 10, 'pendrive kingston', 15000),
(12, 10, 'mouse microsoft', 30000),
(13, 10, 'teclado microsoft', 15000),
(14, 10, 'ipad', 160000),
(15, 10, 'apple tv', 140000),
(16, 10, 'lcd sony', 300000),
(17, 10, 'xbox 360', 170000),
(18, 10, 'playstation 2', 100000),
(19, 10, 'mp3 sony', 25000),
(20, 10, 'mp4 toshiba', 19000),
(21, 10, 'videocamara sony', 270000),
(22, 10, 'camara canon', 330000),
(23, 10, 'lente canon', 80000),
(24, 10, 'led samsung', 380000),
(25, 10, 'imac', 890000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
